provider "aws" {
  region = "us-east-1"
  access_key = "XXX"
  secret_key = "YYY"
  profile = "default"
}

resource "aws_security_group" "web_traffic" {
  name        = "Allow web traffic"
  description = "Allow ssh and standard http/https ports inbound and everything outbound"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8888
    to_port     = 8888
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "microsservice-order" {  
  ami = "ami-04d29b6f966df1537" //Amazon Linux 2 AMI
  instance_type = "t2.micro"
  key_name = "danilons"  
  
  provisioner "file" {
    source      = "install.sh"
    destination = "/home/ec2-user/install.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "export AWS_ACCOUNT_ID=ZZZ",
      "export AWS_ACCESS_KEY_ID=XXX",
      "export AWS_SECRET_ACCESS_KEY=YYY",
      "export AWS_DEFAULT_REGION=us-east-1",
      "aws configure",
      "sudo yum update -y",
      "sudo amazon-linux-extras install docker",
      "sudo service docker start",
      "sudo usermod -a -G docker ec2-user",
      "sudo systemctl enable docker",
      "docker info",
      "chmod +x /home/ec2-user/install.sh"
    ]
  } 

  connection {
    type        = "ssh"
    host        = "${self.public_dns}"
    user        = "ec2-user"
    private_key = "${file("danilons.pem")}"
  }

  tags = { 
    name = "order" 
  }
  
}